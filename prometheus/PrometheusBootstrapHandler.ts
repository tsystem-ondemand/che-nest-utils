import {
  Inject, Injectable, OnApplicationBootstrap,
} from '@nestjs/common';
import { DefaultMetricsCollectorConfiguration } from 'prom-client';
import DetachedPrometheusServer from './DetachedPrometheusServer';


@Injectable()
class PrometheusBootstrapHandler implements OnApplicationBootstrap {
  constructor(
    @Inject('DetachedPrometheusServer') private prometheusServer: DetachedPrometheusServer,
    @Inject('collectDefaultMetrics') private collectDefaultMetrics:
    (config?: DefaultMetricsCollectorConfiguration) => ReturnType<typeof setInterval>,
  ) {}

  async onApplicationBootstrap() {
    this.collectDefaultMetrics();
    await this.prometheusServer.run();
  }
}

export default PrometheusBootstrapHandler;
