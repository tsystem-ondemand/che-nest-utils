import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import {
  Histogram, HistogramConfiguration, SummaryConfiguration, Summary,
} from 'prom-client';
import RequestMetrics from './RequestMetrics';

const labelNames = ['method', 'url', 'status'];

const summaryConfiguration: SummaryConfiguration<string> = {
  name: 'http_request_summary_seconds',
  help: 'request duration in seconds summary',
  labelNames,
  percentiles: [0.5, 0.9, 0.95, 0.99],
};

const histogramConfiguration: HistogramConfiguration<string> = {
  name: 'http_request_duration_seconds',
  help: 'request duration in seconds',
  labelNames,
  buckets: [0.05, 0.1, 0.5, 1, 3, 5, 10],
};

const requestMetrics: RequestMetrics = {
  histogram: new Histogram(histogramConfiguration),
  summary: new Summary(summaryConfiguration),
};

const RequestMetricsProvider: Provider = {
  provide: 'requestMetrics',
  useValue: requestMetrics,
};

export default RequestMetricsProvider;
