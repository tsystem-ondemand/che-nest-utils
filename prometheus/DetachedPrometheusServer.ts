import { Inject, Injectable, LoggerService } from '@nestjs/common';
import * as http from 'http';
import { Registry } from 'prom-client';
import PromServerConfig from './types/PromServerConfig';
import { Logger } from '../loggers/appLogger/LoggerDecorator';

@Injectable()
class DetachedPrometheusServer {
  constructor(
    @Inject('PromServerConfig') private config: PromServerConfig,
    @Inject('promClientRegister') private promClientRegister: Registry,
    @Logger('DetachedPrometheusServer') private logger: LoggerService,
  ) {}

  async run() {
    const { port } = this.config;
    const requestHandler = this.getRequestHandler();
    const server = http.createServer(requestHandler);

    server.on('listening', () => {
      this.logger.log(`Listening on port ${port}`);
    });

    server.on('error', (error) => {
      this.logger.error(error);
    });

    server.listen(port);
  }

  private sendPlainText(response: http.ServerResponse) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end(this.promClientRegister.metrics());
  }

  private sendJson(response: http.ServerResponse) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end(this.promClientRegister.getMetricsAsJSON());
  }

  private getRequestHandler(): http.RequestListener {
    return (
      { url = '' }: http.IncomingMessage,
      response: http.ServerResponse,
    ) => (url.toLocaleLowerCase().endsWith('json')
      ? this.sendJson(response)
      : this.sendPlainText(response));
  }
}

export default DetachedPrometheusServer;
