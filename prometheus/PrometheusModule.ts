import { Module } from '@nestjs/common';
import * as promClient from 'prom-client';
import DetachedPrometheusServer from './DetachedPrometheusServer';
import config from './config';
import PrometheusInterceptor from './PrometheusInterceptor';
import PrometheusBootstrapHandler from './PrometheusBootstrapHandler';
import RequestMetricsProvider from './RequestMetricsProvider';


@Module({
  imports: [],
  providers: [
    {
      provide: 'PromServerConfig',
      useValue: config,
    },
    {
      provide: 'DetachedPrometheusServer',
      useClass: DetachedPrometheusServer,
    },
    {
      provide: 'collectDefaultMetrics',
      useValue: promClient.collectDefaultMetrics,
    },
    {
      provide: 'promClientRegister',
      useValue: promClient.register,
    },

    RequestMetricsProvider,
    PrometheusBootstrapHandler,
    PrometheusInterceptor,
  ],
  exports: ['PrometheusInterceptor', 'collectDefaultMetrics', 'promClientRegister', 'requestMetrics'],
})
class PrometheusModule {}


export default PrometheusModule;
