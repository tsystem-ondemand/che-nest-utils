import {
  Injectable, NestInterceptor, ExecutionContext, CallHandler, Inject,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Histogram, Summary } from 'prom-client';
import { tap } from 'rxjs/operators';
import { FastifyRequest } from 'fastify';
import RequestMetrics from './RequestMetrics';


@Injectable()
class PrometheusInterceptor implements NestInterceptor {
  private histogram: Histogram;

  private summary: Summary;

  constructor(@Inject('requestMetrics') requestMetrics: RequestMetrics) {
    this.histogram = requestMetrics.histogram;
    this.summary = requestMetrics.summary;
  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const stopSummaryTimer = this.summary.startTimer();
    const stopHistogramTimer = this.histogram.startTimer();

    return next.handle()
      .pipe(
        tap(() => {
          const {
            req: {
              url = 'UNKNOWN_URL',
              method = 'UNKNOWN_METHOD',
              statusCode,
            },
          }: FastifyRequest = context.switchToHttp().getRequest();

          stopSummaryTimer({ url, method, statusCode: statusCode || 'UNKNOWN_STATUS' });
          stopHistogramTimer({ url, method, statusCode: statusCode || 'UNKNOWN_STATUS' });
        }),
      );
  }
}

export default PrometheusInterceptor;
