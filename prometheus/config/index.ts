import ConfigurationHelper from '../../configuration/ConfigurationHelper';
import PromServerConfig from '../types/PromServerConfig';

const config = ConfigurationHelper.getEnvVariables<PromServerConfig>({
  port: { name: 'PROMETHEUS_PORT', transform: Number, defaultValue: 9090 },
});

export default config;
