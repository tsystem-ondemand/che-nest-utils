import { Provider } from '@nestjs/common';
import { prefixesForLoggers } from './LoggerDecorator';
import LoggerService from './LoggerService';


function setLoggerPrefix(logger: LoggerService, prefix: string) {
  if (prefix) {
    logger.setPrefix(prefix);
  }

  return logger;
}

function createLoggerProvider(prefix: string): Provider<LoggerService> {
  return {
    provide: `LoggerService${prefix}`,
    useFactory: (logger) => setLoggerPrefix(logger, prefix),
    inject: [LoggerService],
  };
}


export default function createLoggerProviders(): Array<Provider<LoggerService>> {
  return prefixesForLoggers.map(createLoggerProvider);
}
