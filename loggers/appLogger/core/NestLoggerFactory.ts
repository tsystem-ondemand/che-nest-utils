import { LoggerService } from '@nestjs/common';
import LogLevel from './LogLevel';
import NestLogger from './NestLogger';


class NestLoggerFactory {
  public static getLogger(logLevel?: LogLevel): LoggerService {
    const envLogLevel = LogLevel.fromString(process.env.LOG_LEVEL);

    return new NestLogger(logLevel || envLogLevel);
  }
}

export default NestLoggerFactory;
