enum LogLevel {
  fatal,
  error,
  warn,
  info,
  debug,
  trace,
}

function getLevelFromString(level?: string): LogLevel {
  switch (level) {
    case 'fatal':
      return LogLevel.fatal;
    case 'error':
      return LogLevel.error;
    case 'warn':
      return LogLevel.warn;
    case 'debug':
      return LogLevel.debug;
    case 'trace':
      return LogLevel.trace;
    case 'info':
    default:
      return LogLevel.info;
  }
}

// eslint-disable-next-line no-redeclare
namespace LogLevel {
  export const fromString = getLevelFromString;
}

export default LogLevel;
