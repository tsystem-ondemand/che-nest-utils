import LogLevel from '../LogLevel';
import NestLogger from '../NestLogger';
import ConsoleLogger from '../ConsoleLogger';

jest.mock('../ConsoleLogger');

describe('Nest logger', () => {
  let nestLogger: NestLogger;

  beforeEach(async () => {
    nestLogger = new NestLogger(LogLevel.trace);
  });

  describe('Method log', () => {
    it('call super.info"', async () => {
      const message = 'example';

      nestLogger.log(message);

      expect(ConsoleLogger.prototype.info).toHaveBeenCalled();
      expect(ConsoleLogger.prototype.info)
        .toHaveBeenCalledWith(message);
    });
  });

  describe('Method error', () => {
    it('call super.error"', async () => {
      const message = 'example';
      const trace = 'trace';

      nestLogger.error(message, trace);

      expect(ConsoleLogger.prototype.error).toHaveBeenCalled();
      expect(ConsoleLogger.prototype.error)
        .toHaveBeenCalledWith(message, trace);
    });
  });

  describe('Method warn', () => {
    it('call super.warn"', async () => {
      const message = 'example';

      nestLogger.warn(message);

      expect(ConsoleLogger.prototype.warn).toHaveBeenCalled();
      expect(ConsoleLogger.prototype.warn)
        .toHaveBeenCalledWith(message);
    });
  });

  describe('Method debug', () => {
    it('call super.debug"', async () => {
      const message = 'example';

      nestLogger.debug(message);

      expect(ConsoleLogger.prototype.debug).toHaveBeenCalled();
      expect(ConsoleLogger.prototype.debug)
        .toHaveBeenCalledWith(message);
    });
  });

  describe('Method verbose', () => {
    it('call super.trace"', async () => {
      const message = 'example';

      nestLogger.verbose(message);

      expect(ConsoleLogger.prototype.trace).toHaveBeenCalled();
      expect(ConsoleLogger.prototype.trace)
        .toHaveBeenCalledWith(message);
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
