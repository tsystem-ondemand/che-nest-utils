import { DynamicModule, Global } from '@nestjs/common';
import { ValueProvider } from '@nestjs/common/interfaces';
import LoggerService from './LoggerService';
import createLoggerProviders from './LoggerProvider';
import LogLevel from './core/LogLevel';

@Global()
class LoggerModule {
  static get(): DynamicModule {
    const prefixedLoggerProviders = createLoggerProviders();

    const logLevel: LogLevel = LogLevel.fromString(process.env.LOG_LEVEL);

    const logLevelValueProvider : ValueProvider = {
      provide: 'LogLevel',
      useValue: logLevel,
    };

    return {
      module: LoggerModule,
      providers: [logLevelValueProvider, LoggerService, ...prefixedLoggerProviders],
      exports: [LoggerService, ...prefixedLoggerProviders],
    };
  }
}

export default LoggerModule;
