import {
  Inject,
  Injectable,
  LoggerService as LoggerServiceInterface,
  Scope,
} from '@nestjs/common';
import ConsoleLogger from './core/ConsoleLogger';
import LogLevel from './core/LogLevel';


@Injectable({
  scope: Scope.TRANSIENT,
})
class LoggerService extends ConsoleLogger implements LoggerServiceInterface {
  private prefix?: string = '';

  constructor(@Inject('LogLevel') logLevel: LogLevel) {
    super(logLevel);
  }

  public setPrefix(prefix: string) {
    this.prefix = prefix;
  }

  log(message: string) {
    super.info(this.prefix, message);
  }

  error(message: string, trace?: string) {
    super.error(this.prefix, message, trace);
  }

  warn(message: string) {
    super.warn(this.prefix, message);
  }

  debug(message: string) {
    super.debug(this.prefix, message);
  }

  verbose(message: string) {
    super.trace(this.prefix, message);
  }
}

export default LoggerService;
