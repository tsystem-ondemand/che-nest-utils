import { Global, Module } from '@nestjs/common';
import AccessLogger from './AccessLogger';
import AccessLoggerNestMiddleware from './AccessLoggerNestMiddleware';

@Global()
@Module({
  providers: [
    AccessLogger,
    AccessLoggerNestMiddleware,
  ],
  exports: [
    AccessLoggerNestMiddleware,
  ],
})
class AccessLoggerModule {}

export default AccessLoggerModule;
