import { LoggerService } from '@nestjs/common';
import { Logger } from '../appLogger/LoggerDecorator';

class AccessLogger {
  constructor(@Logger('AccessLogger') private logger: LoggerService) {}

  log(
    method?: string,
    url?: string,
    statusCode?: number,
    statusMessage?: string,
  ) {
    const requestString = `\nAPI request\nMethod: ${method}\nURL: ${url}\nStatus: ${statusCode} - ${statusMessage}\n`;

    this.logger.log(requestString);
  }
}

export default AccessLogger;
