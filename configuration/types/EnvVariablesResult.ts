export default interface EnvVariablesResult {
  [key: string]: any;
}
