export default interface Paginated<T> {
  items: Array<T>;
  total: number;
}
